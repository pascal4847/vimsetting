set nu
set hlsearch
set ts=4
set cindent
set ruler
set showmatch
set shiftwidth=4
set rtp+=~/.vim/bundle/Vundle.vim
set complete=.,w,b,u,t,i,
filetype plugin indent on

call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'AutoComplPop'
call vundle#end()
let g:acp_completeOption = '.,w,b,u,i,t' 

function! StartVim()
	NERDTree	" open NERDTREE
	wincmd p	" cursor to previous(origin)
endfunction
autocmd VimEnter * call StartVim()	" set init function

function! EnterBuf()
	 if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
endfunction
autocmd bufenter * call EnterBuf()	" close if NERTREE is last window

nmap <F2> :NERDTree<CR>
let NERDTreeWinPos = "right"
